package org.midorinext.android.js

import com.anthonycr.mezzanine.FileStream


@FileStream("app/src/main/js/MidoriBlock.js")
interface MidoriBlock {

    fun provideJs(): String

}
