package org.midorinext.android.intro

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AbsListView.CHOICE_MODE_SINGLE
import android.widget.AdapterView.OnItemClickListener
import androidx.fragment.app.Fragment
import com.github.appintro.SlidePolicy
import org.midorinext.android.AppTheme
import org.midorinext.android.R
import org.midorinext.android.di.injector
import org.midorinext.android.preference.UserPreferences
import org.midorinext.android.search.SearchEngineProvider
import org.midorinext.android.search.engine.BaseSearchEngine
import javax.inject.Inject

class SearchEngineFragment : Fragment(), SlidePolicy {
    @Inject
    lateinit var searchEngineProvider: SearchEngineProvider

    @Inject
    lateinit var userPreferences: UserPreferences

    private lateinit var checkBox: CheckBox

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.search_choice, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //checkBox = view.findViewById(R.id.check_box)
        var col: Int? = null
        when (userPreferences.useTheme) {
            AppTheme.LIGHT -> col = Color.WHITE
            AppTheme.DARK -> col = Color.BLACK
            AppTheme.BLACK -> col = Color.BLACK
        }
        var textCol: Int? = null
        when (userPreferences.useTheme) {
            AppTheme.LIGHT -> textCol = Color.BLACK
            AppTheme.DARK -> textCol = Color.WHITE
            AppTheme.BLACK -> textCol = Color.WHITE
        }

        if (col != null) {
            requireView().setBackgroundColor(col)
        }
        if (textCol != null) {
            requireView().findViewById<TextView>(R.id.textView4).setTextColor(textCol)
        }

        val listView = activity?.findViewById<ListView>(R.id.engines)
        val values = convertSearchEngineToString(searchEngineProvider.provideAllSearchEngines())
        val arrayAdapter = ArrayAdapter(activity as Context, android.R.layout.simple_list_item_single_choice, values.drop(1))
        listView?.adapter = arrayAdapter
        listView?.choiceMode = CHOICE_MODE_SINGLE
        listView?.setItemChecked(0, true)

        listView!!.onItemClickListener = OnItemClickListener { adapterView, view, i, l ->
            userPreferences.searchChoice = searchEngineProvider.mapSearchEngineToPreferenceIndex(searchEngineProvider.provideAllSearchEngines()[i+1])
        }
    }

    private fun convertSearchEngineToString(searchEngines: List<BaseSearchEngine>): Array<String> =
            searchEngines.map { getString(it.titleRes) }.toTypedArray()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        injector.inject(this)
    }

    override val isPolicyRespected: Boolean
        get() = true //checkBox.isChecked

    override fun onUserIllegallyRequestedNextPage() {
        Toast.makeText(
                requireContext(),
                R.string.tabs,
                Toast.LENGTH_SHORT
        ).show()
    }

    companion object {
        fun newInstance() : SearchEngineFragment {
            return SearchEngineFragment()
        }
    }
}