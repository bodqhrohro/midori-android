package org.midorinext.android.intro

import android.Manifest
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import androidx.fragment.app.Fragment
import com.github.appintro.AppIntro2
import com.github.appintro.AppIntroFragment
import javax.inject.Inject
import org.midorinext.android.AppTheme
import org.midorinext.android.MainActivity
import org.midorinext.android.R
import org.midorinext.android.di.injector
import org.midorinext.android.preference.UserPreferences

class WelcomeIntro : AppIntro2(){

    @Inject
    lateinit var userPreferences: UserPreferences

    lateinit var preference : SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        injector.inject(this)

        setTheme(when (userPreferences.useTheme) {
            AppTheme.LIGHT -> R.style.Theme_LightTheme
            AppTheme.DARK -> R.style.Theme_DarkTheme
            AppTheme.BLACK -> R.style.Theme_BlackTheme
            AppTheme.RED -> R.style.Theme_RedTheme
            AppTheme.BLUE -> R.style.Theme_BlueTheme
            AppTheme.GREEN -> R.style.Theme_GreenTheme
            AppTheme.YELLOW -> R.style.Theme_YellowTheme
        })

        super.onCreate(savedInstanceState)
        showSlide()
    }

    private fun showSlide(){
        isWizardMode = true

        var col: Int
        when (userPreferences.useTheme) {
            AppTheme.LIGHT -> Color.WHITE
            AppTheme.DARK -> Color.BLACK
            AppTheme.BLACK -> Color.BLACK
            AppTheme.RED -> Color.RED
            AppTheme.GREEN -> Color.GREEN
            AppTheme.BLUE -> Color.BLUE
            AppTheme.YELLOW -> Color.YELLOW
        }

        var textCol: Int
        when (userPreferences.useTheme) {
            AppTheme.LIGHT -> Color.BLACK
            AppTheme.DARK -> Color.WHITE
            AppTheme.BLACK -> Color.WHITE
            AppTheme.RED -> Color.RED
            AppTheme.GREEN -> Color.GREEN
            AppTheme.BLUE -> Color.BLUE
            AppTheme.YELLOW -> Color.YELLOW

        }

        val a = TypedValue()
        theme.resolveAttribute(android.R.attr.windowBackground, a, true)

        addSlide(AppIntroFragment.newInstance(
                title = resources.getString(R.string.welcome),
                backgroundColor = Color.parseColor("#ffffff"),
                titleColor = Color.parseColor("#12a90b"),
                descriptionColor = Color.parseColor("#12a90b"),
                imageDrawable = R.drawable.slide1,
                description = resources.getString(R.string.app_desc)
        ))

        addSlide(PermsFragment.newInstance())

        addSlide(SearchEngineFragment.newInstance())

        addSlide(ThemeChoiceFragment.newInstance())

        addSlide(NavbarChoiceFragment.newInstance())

        askForPermissions(
                permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                slideNumber = 2,
                required = true)

        isIndicatorEnabled = true
        setIndicatorColor(
                selectedIndicatorColor = (Color.RED),
                unselectedIndicatorColor = (Color.GRAY)
        )
        setProgressIndicator()
    }

    private fun main()
    {
        startActivity(Intent(this, MainActivity::class.java))
    }

    override fun onSkipPressed(currentFragment: Fragment?) {
        super.onSkipPressed(currentFragment)
        main()
        finish()
    }

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        // Decide what to do when the user clicks on "Done"
        main()
        finish()
    }

    override fun onSlideChanged(oldFragment: Fragment?, newFragment: Fragment?) {
        super.onSlideChanged(oldFragment, newFragment)
        Log.d("Hello", "Changed")
    }

    override fun onIntroFinished() {
        super.onIntroFinished()
    }

    companion object {
        private const val PRIVATE_MODE = 0
        private const val PREFERENCE_CONFIGURATION_NAME = "configuration"
        private const val FIRST_TIME = "isFirstRun"
    }
}
