package org.midorinext.android.intro

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.github.appintro.SlidePolicy
import org.midorinext.android.AppTheme
import org.midorinext.android.R
import org.midorinext.android.di.injector
import org.midorinext.android.preference.UserPreferences
import org.midorinext.android.search.SearchEngineProvider
import javax.inject.Inject

class NavbarChoiceFragment : Fragment(), SlidePolicy {
    @Inject
    lateinit var searchEngineProvider: SearchEngineProvider

    @Inject
    lateinit var userPreferences: UserPreferences

    private lateinit var checkBox: CheckBox

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.navbar_choice, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //checkBox = view.findViewById(R.id.check_box)

        var col: Int?
        col = null
        when (userPreferences.useTheme) {
            AppTheme.LIGHT -> col = Color.WHITE
            AppTheme.DARK -> col = Color.BLACK
            AppTheme.BLACK -> col = Color.BLACK
        }
        var textCol: Int?
        textCol = null
        when (userPreferences.useTheme) {
            AppTheme.LIGHT -> textCol = Color.BLACK
            AppTheme.DARK -> textCol = Color.WHITE
            AppTheme.BLACK -> textCol = Color.WHITE
        }

        if (col != null) {
            requireView().setBackgroundColor(col)
        }
        if (textCol != null) {
            requireView().findViewById<TextView>(R.id.textView4).setTextColor(textCol)
        }

        val rGroup = getView()?.findViewById(R.id.radioGroup) as RadioGroup
        rGroup.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.defaultNavbar -> userPreferences.bottomBar = false
                R.id.defaultNavbar2nd -> {
                    userPreferences.bottomBar = false
                    userPreferences.navbar = true
                }
                R.id.bottomNavbar -> userPreferences.bottomBar = true
            }
        }
        val rGroup2 = getView()?.findViewById(R.id.radioGroup2) as RadioGroup
        rGroup2.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.defaultTabs -> userPreferences.showTabsInDrawer = true
                R.id.fullTabs -> userPreferences.showTabsInDrawer = false
            }
        }


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injector.inject(this)
    }

    override val isPolicyRespected: Boolean
        get() = true //checkBox.isChecked

    override fun onUserIllegallyRequestedNextPage() {
        Toast.makeText(
                requireContext(),
                R.string.tabs,
                Toast.LENGTH_SHORT
        ).show()
    }

    companion object {
        fun newInstance() : NavbarChoiceFragment {
            return NavbarChoiceFragment()
        }
    }
}