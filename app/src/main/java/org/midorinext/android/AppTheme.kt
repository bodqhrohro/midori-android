package org.midorinext.android

import org.midorinext.android.preference.IntEnum

/**
 * The available app themes.
 */
enum class AppTheme(override val value: Int) : IntEnum {
    LIGHT(0),
    DARK(1),
    BLACK(2),
    GREEN(3),
    BLUE(4),
    YELLOW(5),
    RED(6)
}
