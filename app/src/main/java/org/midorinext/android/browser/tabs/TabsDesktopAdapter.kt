package org.midorinext.android.browser.tabs

import org.midorinext.android.R
import org.midorinext.android.controller.UIController
import org.midorinext.android.utils.ThemeUtils
import org.midorinext.android.utils.Utils
import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.ViewGroup
import androidx.core.widget.TextViewCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import org.midorinext.android.extensions.*
import org.midorinext.android.preference.UserPreferences
import java.util.*

/**
 * The adapter for horizontal desktop style browser tabs.
 */
class TabsDesktopAdapter(
    context: Context,
    private val resources: Resources,
    private val uiController: UIController,
    private val userPreferences: UserPreferences
) : RecyclerView.Adapter<TabViewHolder>() {

    private val backgroundTabDrawable: Drawable?
    private val foregroundTabBitmap: Bitmap?
    private var tabList: List<TabViewState> = emptyList()

    init {
        val backgroundColor = Utils.mixTwoColors(ThemeUtils.getPrimaryColor(context), Color.BLACK, 0.85f)
        val backgroundTabBitmap = Bitmap.createBitmap(
            context.dimen(R.dimen.desktop_tab_width),
            context.dimen(R.dimen.desktop_tab_height),
            Bitmap.Config.ARGB_8888
        ).also {
            Canvas(it).drawTrapezoid(backgroundColor, false)
        }
        backgroundTabDrawable = BitmapDrawable(resources, backgroundTabBitmap)

        val foregroundColor = ThemeUtils.getPrimaryColor(context)
        foregroundTabBitmap = Bitmap.createBitmap(
            context.dimen(R.dimen.desktop_tab_width),
            context.dimen(R.dimen.desktop_tab_height),
            Bitmap.Config.ARGB_8888
        ).also {
            Canvas(it).drawTrapezoid(foregroundColor, false)
        }
    }

    fun showTabs(tabs: List<TabViewState>) {
        val oldList = tabList
        tabList = tabs

        DiffUtil.calculateDiff(TabViewStateDiffCallback(oldList, tabList)).dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): TabViewHolder {
        val view = viewGroup.context.inflater.inflate(R.layout.tab_list_item_horizontal, viewGroup, false)
        return TabViewHolder(view, uiController, userPreferences)
    }

    override fun onBindViewHolder(holder: TabViewHolder, position: Int) {
        holder.exitButton.tag = position

        val web = tabList[position]

        holder.txtTitle.text = web.title
        updateViewHolderAppearance(holder, web.favicon, web.isForegroundTab)
        updateViewHolderFavicon(holder, web.favicon, web.isForegroundTab)
    }

    fun moveItem(from: Int, to: Int){
        val oldList = tabList
        if (from < to) {
            for (i in from until to) {
                Collections.swap(oldList, i, i + 1)
            }
        } else {
            for (i in from downTo to + 1) {
                Collections.swap(oldList, i, i - 1)
            }
        }
        uiController.getTabModel().moveTab(from, to)
        showTabs(oldList)
        notifyItemMoved(from, to)
    }

    private fun updateViewHolderFavicon(viewHolder: TabViewHolder, favicon: Bitmap?, isForeground: Boolean) {
        favicon?.let {
            if (isForeground) {
                viewHolder.favicon.setImageBitmap(it)
            } else {
                viewHolder.favicon.setImageBitmap(it.desaturate())
            }
        } ?: viewHolder.favicon.setImageResource(R.drawable.ic_webpage)
    }

    private fun updateViewHolderAppearance(viewHolder: TabViewHolder, favicon: Bitmap?, isForeground: Boolean) {
        if (isForeground) {
            val foregroundDrawable = BitmapDrawable(resources, foregroundTabBitmap)
            if (uiController.isColorMode()) {
                foregroundDrawable.tint(uiController.getUiColor())
            }
            TextViewCompat.setTextAppearance(viewHolder.txtTitle, R.style.boldText)
            viewHolder.layout.background = foregroundDrawable
            uiController.changeToolbarBackground(favicon, foregroundDrawable)
        } else {
            TextViewCompat.setTextAppearance(viewHolder.txtTitle, R.style.normalText)
            viewHolder.layout.background = backgroundTabDrawable
        }
    }

    override fun getItemCount() = tabList.size

}
