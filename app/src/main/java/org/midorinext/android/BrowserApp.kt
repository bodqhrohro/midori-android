package org.midorinext.android

import android.app.Activity
import android.app.Application
import android.app.job.JobInfo
import android.content.Context
import android.os.Build
import android.os.StrictMode
import android.webkit.WebView
import androidx.appcompat.app.AppCompatDelegate
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.plugins.RxJavaPlugins
import org.acra.ACRA
import org.acra.ReportField.*
import org.acra.annotation.AcraCore
import org.acra.annotation.AcraHttpSender
import org.acra.annotation.AcraScheduler
import org.acra.config.ACRAConfigurationException
import org.acra.config.CoreConfigurationBuilder
import org.acra.config.HttpSenderConfigurationBuilder
import org.acra.config.ToastConfigurationBuilder
import org.acra.data.StringFormat
import org.acra.reporter.*
import org.acra.sender.HttpSender
import org.midorinext.android.database.bookmark.BookmarkExporter
import org.midorinext.android.database.bookmark.BookmarkRepository
import org.midorinext.android.device.BuildInfo
import org.midorinext.android.device.BuildType
import org.midorinext.android.di.*
import org.midorinext.android.log.*
import org.midorinext.android.preference.DeveloperPreferences
import org.midorinext.android.utils.FileUtils
import org.midorinext.android.utils.MemoryLeakUtils
import org.midorinext.android.utils.installMultiDex
import javax.inject.Inject
import kotlin.system.exitProcess

@AcraHttpSender(uri = "https://crash.astian.org/report",
        httpMethod = HttpSender.Method.POST,
        basicAuthLogin = "ChuyZmeCEzQN1HtB",
        basicAuthPassword = "6T4upL1UhFYZHjxP")
@AcraScheduler(requiresNetworkType = JobInfo.NETWORK_TYPE_UNMETERED,
        requiresBatteryNotLow = false,
        restartAfterCrash = false)
class BrowserApp : Application() {

    @Inject internal lateinit var developerPreferences: DeveloperPreferences
    @Inject internal lateinit var bookmarkModel: BookmarkRepository
    @Inject @field:DatabaseScheduler    internal lateinit var databaseScheduler: Scheduler
    @Inject internal lateinit var logger: Logger
    @Inject internal lateinit var buildInfo: BuildInfo

    lateinit var applicationComponent: AppComponent

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        val builder = CoreConfigurationBuilder(this)
        builder.setBuildConfigClass(BuildConfig::class.java).setReportFormat(StringFormat.JSON)
                .setEnabled(true)
        builder.getPluginConfigurationBuilder(ToastConfigurationBuilder::class.java).setResText(R.string.bug_report)
                .setEnabled(true)
        builder.getPluginConfigurationBuilder(HttpSenderConfigurationBuilder::class.java)
                .setUri("https://crash.astian.org/report")
                .setHttpMethod(HttpSender.Method.POST)
                .setBasicAuthLogin("ChuyZmeCEzQN1HtB")
                .setBasicAuthPassword("6T4upL1UhFYZHjxP")
                .setEnabled(true)
        builder.setReportContent(
                APP_VERSION_NAME,
                ANDROID_VERSION,
                PACKAGE_NAME,
                REPORT_ID,
                BRAND,
                PHONE_MODEL,
                BUILD,
                STACK_TRACE,
                USER_APP_START_DATE,
                USER_CRASH_DATE,
                USER_EMAIL,
                USER_IP
        )
        try {
            builder.build()
        } catch (e: ACRAConfigurationException) {
            e.printStackTrace()
        }

        ACRA.init(this, builder)

        if (BuildConfig.DEBUG && Build.VERSION.SDK_INT < 21) {
            installMultiDex(context = base)
        }
    }

    override fun onCreate() {
        super.onCreate()
        // Call to AppIntro2
        /**
        PreferenceManager.getDefaultSharedPreferences(this).apply {
            if (!getBoolean(FIRST_TIME, false)){
                val intent = Intent(applicationContext, WelcomeIntro::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
        }
         */

        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build())
            StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build())
        }

        if (Build.VERSION.SDK_INT >= 28) {
            if (getProcessName() == "$packageName:incognito") {
                WebView.setDataDirectorySuffix("incognito")
            }
        }

        val defaultHandler = Thread.getDefaultUncaughtExceptionHandler()

        Thread.setDefaultUncaughtExceptionHandler { thread, ex ->
            if (BuildConfig.DEBUG) {
                FileUtils.writeCrashToStorage(ex)
            }

            if (defaultHandler != null) {
                defaultHandler.uncaughtException(thread, ex)
            } else {
                exitProcess(2)
            }
        }

        RxJavaPlugins.setErrorHandler { throwable: Throwable? ->
            if (BuildConfig.DEBUG && throwable != null) {
                FileUtils.writeCrashToStorage(throwable)
                throw throwable
            }
        }

        applicationComponent = DaggerAppComponent.builder()
                .application(this)
                .buildInfo(createBuildInfo())
                .build()
        injector.inject(this)

        Single.fromCallable(bookmarkModel::count)
                .filter { it == 0L }
                .flatMapCompletable {
                    val assetsBookmarks = BookmarkExporter.importBookmarksFromAssets(this@BrowserApp)
                    bookmarkModel.addBookmarkList(assetsBookmarks)
                }
                .subscribeOn(databaseScheduler)
                .subscribe()
        if (buildInfo.buildType == BuildType.DEBUG) {
            WebView.setWebContentsDebuggingEnabled(true)
        }

        registerActivityLifecycleCallbacks(object : MemoryLeakUtils.LifecycleAdapter() {
            override fun onActivityDestroyed(activity: Activity) {
                logger.log(TAG, "Cleaning up after the Android framework")
                MemoryLeakUtils.clearNextServedView(activity, this@BrowserApp)
            }
        })
    }

    /**
     * Create the [BuildType] from the [BuildConfig].
     */
    private fun createBuildInfo() = BuildInfo(when {
        BuildConfig.DEBUG -> BuildType.DEBUG
        else -> BuildType.RELEASE
    })

    companion object {
        private const val TAG = "BrowserApp"
        private const val FIRST_TIME = "isFirstRun"

        init {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT)
        }
    }

}