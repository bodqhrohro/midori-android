package org.midorinext.android.search.engine

import org.midorinext.android.R

/**
 * A custom search engine.
 */
class CustomSearch(queryUrl: String) : BaseSearchEngine(
    "file:///android_asset/midoribrowser.webp",
    queryUrl,
    R.string.search_engine_custom
)
