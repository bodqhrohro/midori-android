package org.midorinext.android.search.engine

import org.midorinext.android.R

class SearXSearch: BaseSearchEngine(
        "file:///android_asset/searx.webp",
        "https://searx.ch/?q=",
        R.string.search_engine_searx
)