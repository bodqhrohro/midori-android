package org.midorinext.android.search

import org.midorinext.android.di.SuggestionsClient
import org.midorinext.android.log.Logger
import org.midorinext.android.preference.UserPreferences
import org.midorinext.android.search.engine.*
import org.midorinext.android.search.suggestions.*
import android.app.Application
import dagger.Reusable
import io.reactivex.Single
import okhttp3.OkHttpClient
import javax.inject.Inject

/**
 * The model that provides the search engine based
 * on the user's preference.
 */
@Reusable
class SearchEngineProvider @Inject constructor(
    private val userPreferences: UserPreferences,
    @SuggestionsClient private val okHttpClient: Single<OkHttpClient>,
    private val requestFactory: RequestFactory,
    private val application: Application,
    private val logger: Logger
) {

    /**
     * Provide the [SuggestionsRepository] that maps to the user's current preference.
     */
    fun provideSearchSuggestions(): SuggestionsRepository =
        when (userPreferences.searchSuggestionChoice) {
            0 -> GoogleSuggestionsModel(okHttpClient, requestFactory, application, logger, userPreferences)
            1 -> DuckSuggestionsModel(okHttpClient, requestFactory, application, logger, userPreferences)
            2 -> BaiduSuggestionsModel(okHttpClient, requestFactory, application, logger, userPreferences)
            3 -> NaverSuggestionsModel(okHttpClient, requestFactory, application, logger, userPreferences)
            4 -> NoOpSuggestionsRepository()
            else -> GoogleSuggestionsModel(okHttpClient, requestFactory, application, logger, userPreferences)
        }

    /**
     * Provide the [BaseSearchEngine] that maps to the user's current preference.
     */
    fun provideSearchEngine(): BaseSearchEngine =
        when (userPreferences.searchChoice) {
            0 -> CustomSearch(userPreferences.searchUrl)
            1 -> GoogleSearch()
            2 -> AskSearch()
            3 -> BingSearch()
            4 -> YahooSearch()
            5 -> StartPageSearch()
            6 -> StartPageMobileSearch()
            7 -> DuckSearch()
            8 -> DuckLiteSearch()
            9 -> BaiduSearch()
            10 -> YandexSearch()
            11 -> NaverSearch()
            12 -> EcosiaSearch()
            13 -> EkoruSearch()
            14 -> SearXSearch()
            else -> GoogleSearch()
        }

    /**
     * Return the serializable index of of the provided [BaseSearchEngine].
     */
    fun mapSearchEngineToPreferenceIndex(searchEngine: BaseSearchEngine): Int =
        when (searchEngine) {
            is CustomSearch -> 0
            is GoogleSearch -> 1
            is AskSearch -> 2
            is BingSearch -> 3
            is YahooSearch -> 4
            is StartPageSearch -> 5
            is StartPageMobileSearch -> 6
            is DuckSearch -> 7
            is DuckLiteSearch -> 8
            is BaiduSearch -> 9
            is YandexSearch -> 10
            is NaverSearch -> 11
            is EcosiaSearch -> 12
            is EkoruSearch -> 13
            is SearXSearch -> 14
            else -> throw UnsupportedOperationException("Unknown search engine provided: " + searchEngine.javaClass)
        }

    /**
     * Provide a list of all supported search engines.
     */
    fun provideAllSearchEngines(): List<BaseSearchEngine> = listOf(
        CustomSearch(userPreferences.searchUrl),
        GoogleSearch(),
        AskSearch(),
        BingSearch(),
        YahooSearch(),
        StartPageSearch(),
        StartPageMobileSearch(),
        DuckSearch(),
        DuckLiteSearch(),
        BaiduSearch(),
        YandexSearch(),
        NaverSearch(),
        EcosiaSearch(),
        EkoruSearch(),
        SearXSearch()
    )

}
