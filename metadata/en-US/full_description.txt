Midori is a lightweight, fast and secure web browser that protects the privacy and anonymity of our users,
so the navigation is 100% private.

<h3>Block Ads</h3>
With Midori you will be able to block advertisements and trackers, and with this you will have
a faster navigation and more protection for your data.

<h3>Free Software & Open Source</h3>
Midori is a free and open browser in its entirety, you can audit, report and contribute freely and
be part of the development.

<h3>Private</h3>
Midori provides private browsing by implementing privacy features, HTTPS, proxy support, cookie blocking,
blocking of malicious sites & much more.

<h3>Customization</h3>
We are all different, that's why in Midori we provide you with customization options, change the position
of the navigation bar, change the colors, change the icons, everything to suit your taste.

<h3>Lightweight</h3>
We know you want to get the most out of your device, Midori is extremely light so you can get the
most out of it.
